package nl.utwente.di.converterCtoF;

import java.text.DecimalFormat;

public class Converter {
    DecimalFormat df = new DecimalFormat("0.00");

    double getFahrenheit(String temperature) {
        double result = (Double.parseDouble(temperature) * 1.8) + 32;
        return Math.round(result * 100.0) / 100.0;
    }
}
